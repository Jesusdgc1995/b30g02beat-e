const validarArchivo = (req, res, next) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    res.status(400).json({ msg: "No selecciono ningún archivo." });
    return;
  }

  next();
};

module.exports = {
  validarArchivo,
};

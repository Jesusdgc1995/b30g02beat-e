const path = require("path");
const fs = require("fs");

const { Router, response } = require("express");
const { check, param } = require("express-validator");

const { colleccionesPermitidas } = require("../helpers/db-validator");
const { uploadFile } = require("../helpers/upload-file");

const { validarCampos } = require("../middlewares/validar-campos");
const { validarArchivo } = require("../middlewares/validar-archivo");

const Album = require("../album/model");
const Track = require("../tracks/model");
const User = require("../users/model");

const cloudinary = require("cloudinary").v2;

const router = Router();
cloudinary.config(process.env.CLOUDINARY_URL);

// Controllers
const actualizarImagenCloudinary = async (req, res = response) => {
  const { id, collection } = req.params;

  let model;
  let secure_url;
  const { tempFilePath } = req.files.file;
  switch (collection) {
    case "users":
      model = await User.findById(id);
      if (!model) {
        return res.status(400).json({
          msg: `No existe un usuario con el id ${id}`,
        });
      }

      if (model.photo) {
        const nombreArr = model.photo.split("/");
        const nombre = nombreArr[nombreArr.length - 1];
        console.log(nombre);
        const [public_id] = nombre.split(".");
        await cloudinary.uploader.destroy(public_id);
      }

      secure_url = await (
        await cloudinary.uploader.upload(tempFilePath)
      ).secure_url;
      model.photo = secure_url;
      await model.save();
      break;
    case "albums":
      model = await Album.findById(id);
      if (!model) {
        return res.status(400).json({
          msg: `No existe un album con el id ${id}`,
        });
      }

      if (model.coverUrl) {
        const nombreArr = model.coverUrl.split("/");
        const nombre = nombreArr[nombreArr.length - 1];
        const [public_id] = nombre.split(".");
        await cloudinary.uploader.destroy(public_id);
      }
      secure_url = await (
        await cloudinary.uploader.upload(tempFilePath)
      ).secure_url;
      model.coverUrl = secure_url;
      await model.save();
      break;
    default:
      return res
        .status(500)
        .json({ msg: `La colección ${collection} no esta validada.` });
  }

  res.json({ id, collection });
};

const actualizarAudio = async (req, res = response) => {
  const { id, collection } = req.params;

  let model;
  let nombre;
  switch (collection) {
    case "tracks":
      model = await Track.findById(id);
      if (!model) {
        return res.status(400).json({
          msg: `No existe una pista con el id ${id}`,
        });
      }

      if (model.fileUrl) {
        const pathImagen = path.join(
          __dirname,
          "..\\..\\uploads\\",
          collection,
          model.fileUrl
        );
        if (fs.existsSync(pathImagen)) {
          fs.unlinkSync(pathImagen);
        }
      }

      nombre = await uploadFile(req.files, ["mp3", "m4a", "m4r"], collection);
      model.fileUrl = nombre;
      await model.save();
      break;
    default:
      return res
        .status(500)
        .json({ msg: `La colección ${collection} no esta validada.` });
  }

  res.json({ id, collection });
};

const mostrarImagen = async (req, res = response) => {
  const { id, collection } = req.params;

  let model;
  switch (collection) {
    case "users":
      model = await User.findById(id);
      if (!model) {
        return res.status(400).json({
          msg: `No existe un usuario con el id ${id} - Regresar una imagen por defecto.`,
        });
      }

      if (model.photo) {
        return res.json({ photo: model.photo});
      }
      break;
    case "albums":
      model = await Album.findById(id);
      if (!model) {
        return res.status(400).json({
          msg: `No existe un album con el id ${id} - Regresar una imagen por defecto?`,
        });
      }

      if (model.coverUrl) {
        return res.json({coverUrl: model.coverUrl});
      }
      break;
    default:
      return res
        .status(500)
        .json({ msg: `La colección ${collection} no esta validada.` });
  }
  const pathImagen = path.join(__dirname, "../../assets/no-image.jpg");

  res.sendFile(pathImagen);
};

const obtenerAudio = async (req, res = response) => {
  const { id, collection } = req.params;

  let model;
  switch (collection) {
    case "tracks":
      model = await Track.findById(id);
      if (!model) {
        return res.status(400).json({
          msg: `No existe una pista con el id ${id}`,
        });
      }

      if (model.fileUrl) {
        const pathAudio = path.join(
          __dirname,
          "..\\..\\uploads\\",
          collection,
          model.fileUrl
        );
        if (fs.existsSync(pathAudio)) {
          return res.sendFile(pathAudio);
        }
      }
      break;
    default:
      return res
        .status(500)
        .json({ msg: `La colección ${collection} no esta validada.` });
  }

  res.status(404).json({ msg: "Pista no encontrada" });
};

// Router
router.put(
  "/:collection/:id",
  [
    validarArchivo,
    param("id", "El id debe ser un id de Mongo").isMongoId(),
    param("collection", "Colleccion no permitida").isIn(["users", "albums"]),
    validarCampos,
  ],
  actualizarImagenCloudinary
);
router.get(
  "/:collection/:id",
  [
    check("id", "El id debe ser un id de Mongo").isMongoId(),
    check("collection").custom((c) =>
      colleccionesPermitidas(c, ["users", "albums"])
    ),
    validarCampos,
  ],
  mostrarImagen
);
// Actualizar pistas
router.put(
  "/tracks/:id",
  [
    validarArchivo,
    param("id", "El id debe ser un id de Mongo").isMongoId(),
    param("collection", "Colleccion no permitida").isIn(["tracks"]),
    validarCampos,
  ],
  actualizarAudio
);
// Obtener pista
router.get(
  "/tracks/:id",
  [
    check("id", "El id debe ser un id de Mongo").isMongoId(),
    check("collection").custom((c) => colleccionesPermitidas(c, ["tracks"])),
    validarCampos,
  ],
  obtenerAudio
);

module.exports = router;

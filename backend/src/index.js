require("dotenv").config();
require("./db/db");
require("./config");

const express = require("express");
const expressJwt = require("express-jwt");
const fileUpload = require("express-fileupload");

const cors = require("cors");
const helmet = require("helmet");

const app = express();

app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(express.static("public"));

// app.use(
//   expressJwt({
//     secret: process.env.JSON_SECRET,
//     algorithms: ["HS256"],
//     requestProperty: process.env.requestProperty,
//   }).unless({
//     path: [
//       "/api/login",
//       "/api/register",
//       "/api/uploads/img",
//       "/api/uploads/media",
//     ],
//   })
// );

app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: "/tmp/",
    createParentPath: true
  })
);

app.use("/api/auth", require("./users/auth"));
app.use("/api/user", require("./users"));
app.use("/api/tracks", require("./tracks"));
app.use("/api/albums", require("./album"));
app.use("/api/playlist", require("./playlist"));
app.use("/api/uploads", require("./users/uploads"));

app.listen(process.env.PORT, () => {
  console.log("Corriento puerto " + process.env.PORT);
});

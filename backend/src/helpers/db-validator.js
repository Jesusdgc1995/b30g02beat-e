

const colleccionesPermitidas = (coleccion = '', coleccciones = []) => {
    const incluida = coleccciones.includes(coleccion);
    if(!incluida) {
        throw new Error(`La colección ${coleccion} no es permitida`);
    }

    return true;
}


module.exports = {
    colleccionesPermitidas,
}
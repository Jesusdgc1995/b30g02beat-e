const path = require("path");
const { v4: uuidv4 } = require("uuid");

const uploadFile = (
  files,
  extensionesValidas = ["png", "jpg", "jpeg"],
  collection = ""
) => {
  return new Promise((resolve, reject) => {
    const { file } = files;
    const nombreCortado = file.name.split(".");
    const extension = nombreCortado[nombreCortado.length - 1];

    if (!extensionesValidas.includes(extension)) {
      return reject(`La extension ${extension} no es permitida.`);
    }

    const nombreTemp = uuidv4() + "." + extension;
    const uploadPath = path.join(
      __dirname,
      "..\\..\\uploads\\",
      collection,
      nombreTemp
    );

    file.mv(uploadPath, function (err) {
      if (err) {
        console.log(err);
        return reject(err);
      }

      resolve(nombreTemp);
    });
  });
};

module.exports = {
  uploadFile,
};
